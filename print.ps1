#Script is intended to be stored in C:\attachment-print
#If you prefer another directory, feel free to modify the code below :)
#to execute task silently in background, use C:\attachment-print-rivalta2\task.vbs



#Load ImapX.dll (IMAP client, https://github.com/azanov/imapx )
[Reflection.Assembly]::LoadFile("C:\attachment-print-rivalta2\ImapX.dll")



#Clean downloaded attachment folder, in case some garbage is left there.
Remove-Item C:\attachment-print-rivalta2\attachments\*.* -Force

$Username = ""
$Password = ""
$printer = "Microsoft Print to PDF"

$client = New-Object ImapX.ImapClient
$client.Behavior.MessageFetchMode = "Full"
$client.Host = "imaps.aruba.it"
$client.Port = 993
$client.UseSsl = $true
$client.SslProtocol = [Net.SecurityProtocolType]::Tls12
$client.Connect()
$client.Login($Username, $Password)


#select inbox folder
$res = $client.folders| where { $_.path -eq 'Inbox' }

$trash = $res.subFolders| where { $_.path -eq 'INBOX.Trash' }

# fetch last 100 messages
while(1){
$numberOfMessagesLimit = 100
$messages = $res.search("ALL", $client.Behavior.MessageFetchMode, $numberOfMessagesLimit)

# Display the messages in a formatted table
# $messages | ft *

#download attachements from each unread message ( including inline attachements )
foreach($m in $messages) {
  if($m.Seen) {
  } else {
    write-host($m.Subject)
    foreach($r in $m.Attachments) {
        $r.Download()
        $r.Save('C:\attachment-print-rivalta2\attachments\')
    }
    $m.Flags.Add("\Seen")
    $m.MoveTo($trash)
  }
#mark messages as read
}
#wait 20 seconds, in some cases files appear to late.
timeout 10


#delete empty attechements and files smaller than 1kb
Get-ChildItem "C:\attachment-print-rivalta2\attachments\" -Filter *.stat -recurse |?{$_.PSIsContainer -eq $false -and $_.length -lt 1000}|?{Remove-Item $_.fullname -WhatIf}
Get-ChildItem -Path "C:\attachment-print-rivalta2\attachments\" -Recurse -Force | Where-Object { $_.PSIsContainer -eq $false -and $_.Length -eq 0 } | remove-item
#delete emoticons
Remove-Item C:\attachment-print-rivalta2\attachments\*Emoticon* -Force
Remove-Item C:\attachment-print-rivalta2\attachments\*emoticon* -Force
Remove-Item C:\attachment-print-rivalta2\attachments\*smiley* -Force

#Print PDF files
$FilesToPrint = Get-ChildItem "C:\attachment-print-rivalta2\attachments\*" -Recurse -Include *.pdf,*.PDF
foreach($File in $FilesToPrint) {
    Write-Host $File
    Start-Process -WindowStyle Hidden -FilePath $File.FullName -WorkingDirectory "C:\attachment-print-rivalta2\attachments" -Verb Print -PassThru |Out-Printer -Name $printer| %{ Start-Sleep 15;$_ } | Stop-Process
    Start-Sleep -Seconds 10
}

#move printed files to printed-attachments folder
Get-ChildItem "C:\attachment-print-rivalta2\attachments\*" -Include *.pdf,*.PDF | Move-Item -Force -Destination "C:\attachment-print-rivalta2\printed-attachments\"

#delete garbage
Remove-Item C:\attachment-print-rivalta2\attachments\*.* -Force

#delete printed-attachments older than 60 days
$limit = (Get-Date).AddDays(-60)
Get-ChildItem -Path "C:\attachment-print-rivalta2\printed-attachments\" -Recurse -Force | Where-Object { !$_.PSIsContainer -and $_.CreationTime -lt $limit } | Remove-Item -Force
}
